let faker = require('faker');
let database = { images: []};

for (let i = 1; i<= 600; i++) {
  database.images.push({
    id: i,
    imageUrl: faker.image.avatar(),
    isFavourite: false
  });
}
console.log(JSON.stringify(database));
