import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {ImagesListComponent} from './core/images-list/images-list.component';
import {FavoritesComponent} from './core/favorites/favorites.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HeaderComponent} from './shared/header/header.component';
import {SingleImageComponent} from './core/single-image/single-image.component';
import {ScrollComponent} from './shared/scroll/scroll.component';
import {SpinnerComponent} from './shared/spinner/spinner.component';
import {ImageItemComponent} from './shared/image-item/image-item.component';

@NgModule({
  declarations: [
    AppComponent,
    ImagesListComponent,
    FavoritesComponent,
    HeaderComponent,
    SingleImageComponent,
    ScrollComponent,
    SpinnerComponent,
    ImageItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
