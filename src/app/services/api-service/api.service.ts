import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {retry, catchError, tap, delay} from 'rxjs/operators';
import {LinksInterface} from '../../interfaces/links.interface';

@Injectable({
  providedIn: 'root'
})

export class ApiService {

  private URL = 'http://localhost:3000/images';
  public next = '';

  constructor(
    private httpClient: HttpClient,
  ) {}

  private handleError(error: HttpErrorResponse): Observable<any> {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public getImages(): Observable<HttpResponse<any>> {
    return this.httpClient.get(this.URL, {
      params: new HttpParams({fromString: '_page=1&_limit=12'}), observe: 'response'
    })
      .pipe(
        delay(this.randomInteger(200, 300)),
        retry(3),
        catchError(this.handleError),
        tap(res => {
            this.parseHeader(res.headers.get('Link')
            );
          }
        )
      );
  }

  public parseHeader(header: string): void {
    if (header.length === 0) {
      return;
    }
    const parts = header.split(',');
    const links: LinksInterface = {
      first: '',
      last: '',
      next: ''
    };
    parts.forEach(p => {
      const section = p.split(';');
      const url = section[0].replace(/<(.*)>/, '$1').trim();
      const name = section[1].replace(/rel="(.*)"/, '$1').trim();
      links[name] = url;
    });
    this.next = links.next;
  }

  public getNextImages(url: string): Observable<HttpResponse<any>> {
    return this.httpClient.get(url, {observe: 'response'})
      .pipe(
        delay(this.randomInteger(200, 300)),
        retry(3),
        catchError(this.handleError),
        tap(res => {
          this.parseHeader(res.headers.get('Link'));
        }));
  }

  private randomInteger(min: number, max: number): number {
    const rand: number = min + Math.random() * (max + 1 - min);
    return Math.floor(rand);
  }
}
