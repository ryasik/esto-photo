import { LocalStorageService } from './local-storage.service';
import {TestBed} from '@angular/core/testing';

describe('LocalStorageService', () => {
  let service: LocalStorageService;

  beforeEach(() => {
    let store = {};

    spyOn(localStorage, 'getItem').and.callFake( (key: string): string => {
      return store[key] || null;
    });

    spyOn(localStorage, 'setItem').and.callFake((key: string, value: string): void =>  {
      store[key] = <string>value;
    });
    service = TestBed.inject(LocalStorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set an item', () => {
    expect(service.setItem('foo', 'bar')).toBe(undefined);
    expect(service.getItem('foo')).toBe('bar');
  });

});
