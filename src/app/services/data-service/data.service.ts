import { Injectable } from '@angular/core';
import {ImageInterface} from '../../interfaces/image.interface';
import {LocalStorageService} from '../storage-service/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public chunkImg: ImageInterface[] = [];
  public favoritesImages: ImageInterface[] = [];

  constructor(private localStorageService: LocalStorageService) { }

  // Checking if item is added to favorites images
  public imageIsAdded(image: ImageInterface): boolean {
    return this.favoritesImages.some(img => img.id === image.id);
  }

  public removeFromFavorites(image: ImageInterface): void {
    this.favoritesImages = this.favoritesImages.filter(img => img.id !== image.id);
  }

  // Check and mark images as favorite
  public markFavoriteImages(images: ImageInterface[]): ImageInterface[]  {
    if (!this.localStorageService.getItem('favoritesImages')) {
      return images;
    }
    const favoriteImagesId: number[] = this.localStorageService.getItem('favoritesImages').map(e => e.id);
    return images.map(e => {
      if (favoriteImagesId.includes(e.id)) {
        return {
          ...e,
          isFavourite: true
        };
      }
      return e;
    });
  }
}
