export interface ImageInterface {
  id: number;
  imageUrl: string;
  isFavourite: boolean;
}
