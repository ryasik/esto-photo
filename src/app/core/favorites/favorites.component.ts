import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api-service/api.service';
import {ImageInterface} from '../../interfaces/image.interface';
import {Router} from '@angular/router';
import {LocalStorageService} from '../../services/storage-service/local-storage.service';
import {DataService} from '../../services/data-service/data.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {

  constructor(
    private apiService: ApiService,
    private router: Router,
    private localStorageService: LocalStorageService,
    private dataService: DataService
  ) {}

  favorites: ImageInterface[];

  ngOnInit(): void {
    this.favorites = this.localStorageService.getItem('favoritesImages');
    this.dataService.favoritesImages = this.localStorageService.getItem('favoritesImages');
  }

  // Transition to specific image
  selectImage(product: ImageInterface): void {
    this.router.navigate(['single-image', product.id]);
  }
}
