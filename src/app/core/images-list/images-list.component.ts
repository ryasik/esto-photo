import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApiService} from '../../services/api-service/api.service';
import {HttpResponse} from '@angular/common/http';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ImageInterface} from '../../interfaces/image.interface';
import {LocalStorageService} from '../../services/storage-service/local-storage.service';
import {DataService} from '../../services/data-service/data.service';

@Component({
  selector: 'app-images-list',
  templateUrl: './images-list.component.html',
  styleUrls: ['./images-list.component.scss']
})
export class ImagesListComponent implements OnInit, OnDestroy {
  public images: ImageInterface[] = [];
  public isLoading = false;
  private cancelationSubject$: Subject<void> = new Subject();

  constructor(
    private apiService: ApiService,
    private localStorageService: LocalStorageService,
    private dataService: DataService,
  ) {}

  ngOnInit(): void {
    this.isLoading = true;
    this.apiService.getImages()
      .pipe(
        takeUntil(this.cancelationSubject$)
      ).subscribe((res: HttpResponse<any>) => {
      this.images = this.dataService.markFavoriteImages(res.body);
      this.dataService.chunkImg = this.images;
      this.dataService.favoritesImages = this.localStorageService.getItem('favoritesImages') || [];
      this.isLoading = false;
    });
  }

  // Loading next photos on scroll
  public nextPage(): void {
    if (this.apiService.next !== undefined && this.apiService.next !== '') {
      this.apiService.getNextImages(this.apiService.next)
        .pipe(
          takeUntil(this.cancelationSubject$)
        ).subscribe((res: HttpResponse<any>) => {
        const updatedList = [
          ...this.images,
          ...res.body
        ];
        this.images = updatedList;
        this.dataService.chunkImg = updatedList;
        this.isLoading = false;
        this.images = this.dataService.markFavoriteImages(this.images);
      });
    }
  }

  addImageToFavorites(img: ImageInterface): void {
    const updatedImg = {
      ...img,
      isFavourite: true
    };

    if (!this.dataService.imageIsAdded(updatedImg)) {
      this.dataService.favoritesImages.push(updatedImg);
      this.localStorageService.setItem('favoritesImages', this.dataService.favoritesImages);
      this.images = this.dataService.markFavoriteImages(this.images);
    }
  }

  // Triggering of loading new images on scroll
  onScroll(): void {
    if (this.images.length && !this.isLoading) {
    this.isLoading = true;
    this.nextPage();
    }
  }

  ngOnDestroy(): void {
    this.cancelationSubject$.next();
    this.cancelationSubject$.complete();
  }
}
