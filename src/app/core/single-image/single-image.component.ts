import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api-service/api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ImageInterface} from '../../interfaces/image.interface';
import {LocalStorageService} from '../../services/storage-service/local-storage.service';
import {DataService} from '../../services/data-service/data.service';

@Component({
  selector: 'app-single-image',
  templateUrl: './single-image.component.html',
  styleUrls: ['./single-image.component.scss']
})
export class SingleImageComponent implements OnInit {
  image: ImageInterface;

  constructor(
    private apiService: ApiService,
    private localStorageService: LocalStorageService,
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      const favorites = this.localStorageService.getItem('favoritesImages');

      if (favorites.length) {
        this.image = favorites.filter(e => e.id === +params.id)[0];
      } else {
        this.router.navigate(['favorites']);
      }
    });
  }

  removeFromFavorites(): void {
    this.dataService.removeFromFavorites(this.image);
    this.localStorageService.setItem('favoritesImages', this.dataService.favoritesImages);
    this.image = {
      ...this.image,
      isFavourite: false
    };
  }
}
