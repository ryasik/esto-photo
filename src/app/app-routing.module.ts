import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ImagesListComponent} from './core/images-list/images-list.component';
import {FavoritesComponent} from './core/favorites/favorites.component';
import {SingleImageComponent} from './core/single-image/single-image.component';

const routes: Routes = [
  // { path: '', redirectTo: 'single-image', pathMatch: 'full'},
  { path: '', component: ImagesListComponent },
  { path: 'favorites', component: FavoritesComponent },
  { path: 'single-image/:id', component: SingleImageComponent },
  { path: '**', component: ImagesListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
