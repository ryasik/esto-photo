import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';
import {ImageInterface} from '../../interfaces/image.interface';

@Component({
  selector: 'app-image-item',
  templateUrl: './image-item.component.html',
  styleUrls: ['./image-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageItemComponent {
  @Input() image: ImageInterface;
  @Input() isSingleImage: boolean;
  @Output() imageClicked = new EventEmitter<void>();
}
